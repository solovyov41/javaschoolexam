package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int rowsNumber = (int) ((-1 + Math.sqrt(1. + 8. * inputNumbers.size())) / 2);
        if ((1 + rowsNumber) * rowsNumber / 2 != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort(null);

        int columnsNumber = 2 * rowsNumber - 1;
        int centerCell = (columnsNumber - 1) / 2;
        int[][] pyramid = new int[rowsNumber][columnsNumber];

        int numberPositionInList = 0;
        for (int i = 0; i < rowsNumber; i++) {
            for (int j = centerCell - i; j <= centerCell + i; j += 2) {
                pyramid[i][j] = inputNumbers.get(numberPositionInList++);
            }
        }

        return pyramid;
    }


}
