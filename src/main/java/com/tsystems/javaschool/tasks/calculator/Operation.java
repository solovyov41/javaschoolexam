package com.tsystems.javaschool.tasks.calculator;

import java.util.function.BinaryOperator;

public enum Operation {
    PLUS('+', (a, b) -> a + b, 1),
    SUBTRACTION('-', (a, b) -> a - b, 1),
    MULTIPLICATION('*', (a, b) -> a * b, 2),
    DIVISION('/', (a, b) -> {
        if (b == 0) {
            throw new ArithmeticException("Division by zero");
        } else {
            return a / b;
        }
    }, 2);

    private char sign;
    private BinaryOperator<Double> doubleBinaryOperator;
    private int priority;

    Operation(char sign, BinaryOperator<Double> doubleBinaryOperator, int priority) {
        this.sign = sign;
        this.doubleBinaryOperator = doubleBinaryOperator;
        this.priority = priority;
    }

    public static Operation getBySign(char sign) {
        for (Operation op : Operation.values()) {
            if (op.getSign() == sign) {
                return op;
            }
        }
        throw new UnsupportedOperationException("Unsupported operation: " + sign);
    }

    public static boolean isOperation(char sign) {
        for (Operation op : Operation.values()) {
            if (op.getSign() == sign) {
                return true;
            }
        }
        return false;
    }

    public int getPriority() {
        return priority;
    }

    public char getSign() {
        return sign;
    }

    public Double executeOperation(Double first, Double second) {
        return this.doubleBinaryOperator.apply(first, second);
    }
}
