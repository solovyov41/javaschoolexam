package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.exceptions.ParserException;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        double result;
        try {
            result = calculateRpn(convertToReversePolishNotation(statement));
        } catch (Exception ex) {
            return null;
        }

        DecimalFormatSymbols s = new DecimalFormatSymbols();
        s.setDecimalSeparator('.');
        DecimalFormat f = new DecimalFormat("#.####", s);

        return f.format(result);
    }


    /**
     * Convert input statemnt into reverse polish notation.
     *
     * @param statement input string with an arithmetic expression
     * @return string with arithmetic expression in reverse polish notation
     */
    private String convertToReversePolishNotation(String statement) throws ParserException {
        StringBuilder sbStack = new StringBuilder();
        StringBuilder sbOut = new StringBuilder();
        char cIn;
        char cTmp;

        for (int i = 0; i < statement.length(); i++) {
            cIn = statement.charAt(i);
            if (Operation.isOperation(cIn)) {
                while (sbStack.length() > 0) {
                    cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);

                    if (Operation.isOperation(cTmp) && (Operation.getBySign(cIn).getPriority() <= Operation.getBySign(cTmp).getPriority())) {
                        sbOut.append(" ").append(cTmp).append(" ");
                        sbStack.setLength(sbStack.length() - 1);
                    } else {
                        sbOut.append(" ");
                        break;
                    }
                }
                sbOut.append(" ");
                sbStack.append(cIn);
            } else if ('(' == cIn) {
                sbStack.append(cIn);
            } else if (')' == cIn) {
                cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);

                while ('(' != cTmp) {
                    if (sbStack.length() < 1) {
                        throw new ParserException("Unbalanced parentheses");
                    }
                    sbOut.append(" ").append(cTmp);
                    sbStack.setLength(sbStack.length() - 1);
                    cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);
                }
                sbStack.setLength(sbStack.length() - 1);
            } else {
                sbOut.append(cIn);
            }
        }

        // If there are operators in stack, add them into output string
        while (sbStack.length() > 0) {
            sbOut.append(" ").append(sbStack.substring(sbStack.length() - 1));
            sbStack.setLength(sbStack.length() - 1);
        }

        return sbOut.toString();
    }


    /**
     * Evaluate expression, written in reverse Polish notation
     *
     * @param rpnStatement string with arithmetic expression in reverse polish notation
     * @return double result
     */
    private double calculateRpn(String rpnStatement) throws ParserException {
        double dA;
        double dB;
        String sTmp;
        Deque<Double> stack = new ArrayDeque<>();
        StringTokenizer st = new StringTokenizer(rpnStatement);

        while (st.hasMoreTokens()) {
            sTmp = st.nextToken().trim();
            if (sTmp.length() == 1 && Operation.isOperation(sTmp.charAt(0))) {
                if (stack.size() < 2) {
                    throw new ParserException("Invalid amount of data in the stack for the operation " + sTmp);
                }

                dB = stack.pop();
                dA = stack.pop();
                Operation op = Operation.getBySign(sTmp.charAt(0));

                stack.push(op.executeOperation(dA, dB));
            } else {
                dA = Double.parseDouble(sTmp);
                stack.push(dA);
            }
        }

        if (stack.size() > 1) {
            throw new ParserException("The number of operators does not match the number of operands");
        }

        return stack.pop();
    }

}
