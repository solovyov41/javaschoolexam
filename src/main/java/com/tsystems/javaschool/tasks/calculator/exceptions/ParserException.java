package com.tsystems.javaschool.tasks.calculator.exceptions;

public class ParserException extends Exception{

    private final String errStr;

    public ParserException(String errStr) {
        super();
        this.errStr = errStr;
    }

    @Override
    public String toString(){
        return this.errStr;
    }
}
